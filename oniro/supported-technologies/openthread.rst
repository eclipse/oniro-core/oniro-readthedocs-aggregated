.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

OpenThread
##########

OpenThread is an open-source implementation of the Thread networking protocol
based on IPv6. It is designed for low-powered devices that operate in a mesh
network based on the IEEE 802.15.4 standards.
Some of the main advantages of implementing OpenThread are:

* Simple to install and operate.
* Mandatory authentication of devices.
* All communications are encrypted.
* Self-healing mechanism with no single point of failure in a mesh network.
* Support for low-powered devices.
* Scalable up to hundreds of devices.

For more details on OpenThread, `Click <https://openthread.io/guides/thread-primer>`__.

OpenThread support in Oniro
***************************

The OpenThread open-source project offers a few key components needed to
operate in a Thread network. The core protocol implementation can run on
bare-metal, RTOS or Linux, depending on your use case. Oniro supports
running a Thread node on the Zephyr flavour and a full border router on
the Linux flavour. The following four targets can be build with bitbake and the
Oniro layers:

* OpenThread Border Router (bitbake ot-br-posix)
  A fully fledged border router configuration where you have at least one Thread
  network interface and an additional one to other networks (e.g. Wi-Fi or
  Ethernet), and optionally an internet uplink.

* OpenThread posix service (bitbake ot-daemon)
  For a use case where the device only wants to join a Thread network, but does
  not need to have full border router capabilities. The ot-daemon service runs
  on Linux and offers a standard Linux network interface e.g. `wpan0` to operate
  on.

* Radio Co-Processor firmware (bitbake zephyr-openthread-rcp)
  For the border router configuration, a radio interface is needed to communicate
  with the Thread network. The hardware could be the usual Arduino Nano 33 BLE or
  a USB dongle with the same SoC as the Arduino Nano 33 BLE. To work with the
  OpenThread Border router on the Linux side this special firmware neededs to be
  flashed to the radio device.

* OpenThread node on Zephyr (bitbake zephyr-blueprint-gateway-node)
  The node target is only available if the build includes the Oniro blueprint
  layer. This example application configures various details needed to run
  OpenThread on Zephyr. It can be used as the base for developing an application.

None of the components is part of the default images provided by Oniro. To add
them to your custom image, you would need to define a custom image recipe, or append
to `local.conf` something similar to:

.. code-block:: console

        IMAGE_INSTALL:append = " ot-br-posix"

Oniro Blueprint with OpenThread
*******************************

To demonstrate a turn-key solution with an OpenThread Border Router on Linux,
as well as an OpenThread node based on Zephyr, Oniro implemented a `Transparent
Gateway Blueperint
<https://docs.oniroproject.org/projects/blueprints/transparent-gateway.html>`__.
In the blueprint documentation you can find more details on building and flashing
the components and on-boarding the device with a mobile application.
