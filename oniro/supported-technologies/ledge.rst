.. SPDX-FileCopyrightText: Linaro Ltd
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

LEDGE (Experimental integration)
################################

The `LEDGE group from Linaro <https://www.linaro.org/automotive-iot-and-edge-devices/>`__
is providing a set of reference layers for features as dependable boot, image signing
and more. |main_project_name| is integrating Yocto Project/OpenEmbedded layers provided
by the group. Please note that currently |main_project_name| is providing forks of the
work with some changes required by |main_project_name| that have not been upstreamed yet.

The layers are included by default in all |main_project_name|, while a selected set
of features is enabled.


meta-ts
*******

The `meta-ts layer <https://gitlab.com/Linaro/trustedsubstrate/meta-ts>`__ provides
Yocto Project/OpenEmbedded packaging of the
`Trusted Substrate <https://linaro.atlassian.net/wiki/spaces/TS/overview>`
providing firmware for a selected set of boards, with features like UEFI secure boot,
measured boot with TPM, and dual-banked firmware update with bricking protection.

You can learn more about `meta-ts` features from the
`documentation <https://trs.readthedocs.io/en/latest/firmware/index.html>`__.

meta-ledge-secure
*****************

The `meta-ledg-secure layer <https://gitlab.com/Linaro/trustedsubstrate/meta-ledge-secure>`
provides features like kernel and module signing, configuration of SELinux, software
modules needed to support TPMs.

|main_project_name| currently uses the OPTEE and TPM support packaging from
`meta-ledge-secure`. Other features will be added in the near future.

Other security layers
*********************

`meta-ts` and `meta-ledge-secure` depend on a number of other security layers from the
industry, like `meta-secure-core` and `meta-security`. All those layers are included
in the default build of |main_project_name|.
