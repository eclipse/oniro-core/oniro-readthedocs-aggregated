.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

The docs Repository
===================

.. _docs repository: https://gitlab.eclipse.org/eclipse/oniro-core/docs.git

The `docs repository`_ contains the general documentation of the |main_project_name|
project. The documentation is normally written in reStructuredText, with an
important difference. Unlike in a regular project, the documentation here is not
standalone. Instead, the git repository contains symbolic links that are valid
when an entire workspace is constructed with `oniro
<https://gitlab.eclipse.org/eclipse/oniro-core/oniro>`_  reposiotory.

This complicates the testing process and the deployment process, but allows the
resulting documentation to span multiple repositories, permitting code and text
to be conveniently changed in one go.

Special Jobs
------------

build
.....

The ``build`` job ensures that the documentation can be built with `sphinx-build`
without any warnings or errors. Apart from the complexity of setting up the workspace
as described above, it is fairly typical.

deploy
......

The ``deploy`` job builds aggregated documentation view that is rendered at
https://docs.oniroproject.org/. This job effectively constructs the
workspace as described above and then archives the entire documentation source code,
de-referencing any links that were followed to other repositories, and commits the
updated set of files to helper repository which is observed by readthedocs.

The helper repository is called `oniro-readthedocs-aggregated
<https://gitlab.eclipse.org/eclipse/oniro-core/oniro-readthedocs-aggregated>`_.
That repository contains a webhook, configured at the level of the GitLab project, which
asks readthedocs to re-build the documentation.

Implementation Highlights
-------------------------

Commit Credentials
..................

Commit to the aforementioned aggregated repository is allowed by a personal access token
that is set up in that repository. The value of the token is stored as a protected variable
available to the docs repository.
