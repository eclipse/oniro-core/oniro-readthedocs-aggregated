.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

Releases
########

This section contains information related to releases of |main_project_name|.

.. toctree::
   :maxdepth: 1

   aladeen/0.1.0/release_notes
   jasmine/1.0.0/release_notes
   2.0/2.0.0/index
